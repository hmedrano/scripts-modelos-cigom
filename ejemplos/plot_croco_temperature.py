import sys, os
import netCDF4 as nc 
import xarray as xr 
import datetime as dt
import numpy as np
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from map_plots import map_pcolor


# Cargar Datos
ncSrc = '../../gom-cicese-croco-phy-5km/data/gom-cicese-croco-phy-5km_clim_temperatura_promedio.nc'

dst = nc.Dataset(ncSrc, 'r')
dstLon = dst.variables['longitude'][:] 
dstLat = dst.variables['latitude'][:]
dstTime = nc.num2date(dst.variables['time'][:], dst.variables['time'].units) 
dstVariable = dst.variables['temp'][0,0,:] 
dst.close()


# Graficado
ax, figure = map_pcolor(dstLon, dstLat, dstVariable, title='Temperatura promedio', 
                                                     tickBins={ 'x' : [-98,-95,-92,-89,-86,-83,-80], 
                                                                'y' : [20,22,24,26,28,30] },
                                                     cmap='jet', 
                                                     plot_land=True, 
                                                     vmin=23.0, 
                                                     vmax=29.0)

# Si es necesario agregar leyendas
ax.annotate('Enero', xy=(0.02, 0.94), xycoords='axes fraction')

figure.savefig('pcolormesh_croco_temperature.png', bbox_inches='tight', pad_inches=0.4,dpi=120)



import sys, os
import netCDF4 as nc 
import xarray as xr 
import datetime as dt
import numpy as np
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from map_plots import map_pcolor, map_quiver, add_quiverPlot


# Cargar Datos
ncSrc = '../../gom-cicese-croco-phy-5km/data/gom-cicese-croco-phy-5km_clim_velocidad_promedio.nc'

dst = nc.Dataset(ncSrc, 'r')
dstLon = dst.variables['longitude'][:] 
dstLat = dst.variables['latitude'][:]
dstTime = nc.num2date(dst.variables['time'][:], dst.variables['time'].units) 
dstVariableU = dst.variables['u'][0,0,:] 
dstVariableV = dst.variables['v'][0,0,:] 
speed = np.sqrt(np.power(dstVariableU,2) + np.power(dstVariableV,2))
dst.close()


# Graficado
# pcolormesh
ax, figure = map_pcolor(dstLon, dstLat, speed, title='Climatología anual de las velocidades (magnitud y vectores) a 0m [m/s]', 
                                               tickBins={ 'x' : [-98,-95,-92,-89,-86,-83,-80], 
                                                          'y' : [20,22,24,26,28,30] },
                                               cmap='jet', 
                                               plot_land=True)                                                      
# quiver overlay
add_quiverPlot(ax, dstLon, dstLat, dstVariableU, dstVariableV, quiverkeyunits="ms$^{-1}$)",
                                                               quiverkeysize=.5,
                                                               quiverkeyposition={'x':.47, 'y': 0.2})

figure.savefig('quiver_croco_velocity_magnitude_overlay_vector.png', bbox_inches='tight', dpi=120)



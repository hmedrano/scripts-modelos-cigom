import sys, os
import netCDF4 as nc 
import xarray as xr 
import datetime as dt
import numpy as np
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from map_plots import map_pcolor, map_quiver


# Cargar Datos
ncSrc = '../../gom-cicese-croco-phy-5km/data/gom-cicese-croco-phy-5km_clim_velocidad_promedio.nc'

dst = nc.Dataset(ncSrc, 'r')
dstLon = dst.variables['longitude'][:] 
dstLat = dst.variables['latitude'][:]
dstTime = nc.num2date(dst.variables['time'][:], dst.variables['time'].units) 
dstVariableU = dst.variables['u'][0,0,:] 
dstVariableV = dst.variables['v'][0,0,:] 
dst.close()


# Graficado
ax, figure = map_quiver(dstLon, dstLat, dstVariableU, dstVariableV, 
                                                      title='Vectores de velocidad',
                                                      tickBins={ 'x' : [-98,-95,-92,-89,-86,-83,-80], 
                                                                 'y' : [20,22,24,26,28,30] }, 
                                                      plot_land=True,
                                                      quiverkeyunits="ms$^{-1}$)",
                                                      quiverkeysize=.5,
                                                      quiverkeyposition={'x':.47, 'y': 0.2} )

figure.savefig('quiver_croco_velocity.png', bbox_inches='tight', pad_inches=0.4, dpi=120)



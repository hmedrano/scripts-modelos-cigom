import sys, os
import netCDF4 as nc 
import xarray as xr 
import datetime as dt
import numpy as np
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from map_plots import map_pcolor

'''
 Script que carga datos de archivo netcdf, recupera datos de la variable
 a graficar, coordenadas y utiliza xarray para hacer el promedio mensual
 de los datos.
'''

# Cargar Datos usando xarray
ncSrc = '../../gom-cicese-nemo-g36-phy-3km/data/gom-cicese-nemo-g36-phy-3km_MonClim_votemper.nc'

dst = xr.open_dataset(ncSrc)
dstLon = dst.variables['nav_lon'].values
dstLat = dst.variables['nav_lat'].values
# Calculo de media anual
votemperMean = dst['votemper'].resample(time_counter='1Y').mean()

# Graficado
ax, figure = map_pcolor(dstLon, dstLat, votemperMean[0,0,:], colorbar_label='[°C]', 
                                                             cmap='jet', 
                                                             tickBins={ 'x' : [-98,-95,-92,-89,-86,-83,-80], 
                                                                        'y' : [20,22,24,26,28,30] }, 
                                                             extent=[-98.1, -79.3, 18.1, 30.5], 
                                                             vmin=20, 
                                                             vmax=30)
                                                             
figure.savefig('pcolormesh_golfo36_temperature.png', bbox_inches='tight', pad_inches=0.4,dpi=120)


import sys, os
import netCDF4 as nc 
import xarray as xr 
import datetime as dt
import numpy as np
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from map_plots import map_pcolor

'''
 Script que carga datos de archivo netcdf, recupera datos de la variable
 a graficar, coordenadas y utiliza xarray para hacer el promedio mensual
 de los datos.
'''

# Cargar Datos
ncSrc = '../../nogom-uabc-roms-submes-phy-1-5km/data/CLIM_Temperature_1_100.nc'

dst = nc.Dataset(ncSrc, 'r')
dstLon = dst.variables['lon'][:] 
dstLat = dst.variables['lat'][:]
#
# Las linea anterior recupera el arreglo con las fechas numericas e intenta convertirlas
# a su equivalente datetime pero el archivo netcdf no contiene fechas validas, por lo que 
# En la segunda linea creamos un arreglo de datetimes sintetico
#dstTime = nc.num2date(dst.variables['time'][:], dst.variables['time'].units) 
dstTime = [ dt.datetime(1970,1,day+1) for day in range(0,31) ]

dstVariable = dst.variables['sea_water_potential_temperature'][:] 
dst.close()

# Convertir numpy array a xarray
dstVariableXarray = xr.DataArray(
    data=dstVariable,
    dims=["time", "lat", "lon"],
    coords=dict(
        lon=(["lon"], dstLon),
        lat=(["lat"], dstLat),
        time=dstTime
    ),
    attrs=dict(
        units="degree_c"
    )
)
# Promedio mensual
dstVariableXarrayMean = dstVariableXarray.resample(time='1M').mean()


# Graficado
ax, figure = map_pcolor(dstLon, dstLat, dstVariableXarrayMean, colorbar_label='[°C]', 
                                                               tickBins={ 'x' : [-97,-95,-93,-91], 
                                                                          'y' : [23,25,27,29,30] },
                                                               cmap='jet', 
                                                               plot_land=True)

figure.savefig('pcolormesh_nogom_temperature.png', bbox_inches='tight', pad_inches=0.4,dpi=120)



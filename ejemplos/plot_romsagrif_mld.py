import sys, os
import netCDF4 as nc 
import xarray as xr 
import datetime as dt
import numpy as np
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from map_plots import map_pcolor

'''
 Script que carga datos de archivo netcdf, recupera datos de la variable
 a graficar, coordenadas y utiliza xarray para hacer el promedio mensual
 de los datos.
'''

# Cargar Datos usando xarray
ncSrc = '../../gom-cicese-roms-agrif-phy-5km/data/CLimDiariaMLD.nc'

dst = xr.open_dataset(ncSrc)
dstLon = dst.variables['LON'].values
dstLat = dst.variables['LAT'].values
# Variable tiempo sintetica, y reemplazamos la variable de coordenadas de tiempo
# con la que viene este archivo netcdf, pues no son valores validos, y es la 
# causa de que falle el calculo de la media
sinteticTime = [ dt.datetime(1970,1,1,12) + dt.timedelta(days=day) for day in range(0,365) ]
dstMLD = dst['MIXED_LAYER'].assign_coords(TIME=sinteticTime)

print(dst)
# Calculo de media anual
mldMean = dstMLD.resample(TIME='1Y').mean()

# Graficado
ax, figure = map_pcolor(dstLon, dstLat, mldMean[0,:], colorbar_label='[m]', 
                                                      tickBins={ 'x' : [-98,-95,-92,-89,-86,-83,-80], 
                                                                 'y' : [20,22,24,26,28,30] }, 
                                                      cmap='jet')
                                                      
figure.savefig('pcolormesh_romsagrif_mld.png', bbox_inches='tight', pad_inches=0.4,dpi=120)


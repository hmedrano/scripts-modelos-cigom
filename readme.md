# Script para creación figuras modelos

Este repositorio contiene el script para la creación de figuras de modelos númericos del grupo de modelación CIGOM.
En el script se publican 3 funciones principales que utilizan matplotlib y cartopy para la creación de graficos:

 - **map_pcolor** : Crea un mapa con un grafico tipo pcolormesh a partir de una variable ndarray 2D y variables de coordenadas lat,lon
 - **map_quiver** : Crea un mapa con un grafico tipo quiver a partir de los componentes U y V , variables ndarray 2D y variables de coordenadas lat,lon
 - *add_quiverPlot* : Agrega la capa de vectores (quiver) a una figura existente.

Los dos primeras funciones agregan lineas de costas en alta definición de alguna de las siguientes bases de datos:

 - GSHHS (DEFAULT)  (https://www.ngdc.noaa.gov/mgg/shorelines/gshhs.html)
 - NaturalEarth     (https://www.naturalearthdata.com/)

De solicitarlo para el grafico se agregan lineas batimetricas para las profundidades: 200, 1000, 2000, 3000, 4000, 5000 mts.  Estos datos son descargados de la base de datos NAturalEarth (https://www.naturalearthdata.com/downloads/10m-physical-vectors/10m-bathymetry/)

Tanto la linea de costas y lineas batimetricas son automaticamente descargadas la primera ocasión que se ejecuta la función, dependiendo de la velocidad de conexión puede tomar algunos minutos.

Los metodos permite personalizar el gráfico con titulo, etiqueta de barra de color, mapa de barra de color, número de marcadores ejes, extensión del mapa, etc. 

## Requerimientos

- Python > 3 
- Bibliotecas matplotlib, netCDF4, cartopy, xarray 

### Instalación de requerimientos

1. Instalar una distribución python, se recomienda [Anaconda Python](https://docs.anaconda.com/anaconda/install/)
2. Utilizar un manejador de paquetes para instalar bibliotecas

```bash
# Se recomienda crear un ambiente virtual que contenga estas 
# bibliotecas, ademas de comenzar con una version de python mas reciente.
$ conda create -n cartoplot
$
$ conda activate cartoplot 
(cartoplot)$ conda install matplotlib netCDF4 cartopy xarray

```

### Usuarios cluster CHAMAN

Para los usuarios del cluster chaman que deseen utilizarlo para generar figuras, pueden utilizar el modulo `anaconda-python-3.5` y el ambiente virtual `cartoplot` que ya esta instalado.

Para cargar el ambiente, escribir lo siguiente en la terminal

```bash

# Desmontar cualquier otro modulo cargado
$ module purge

# Cargar modulo y ambiente virtual 
$ module load anaconda-python-3.5
$ source activate cartoplot

```

Con lo anterior el interprete de python que queda en el ambiente tiene los requerimientos instalados para utilizar estos scripts. 


### Ejemplos

En la carpeta `ejemplos` se encuentran scripts que abren archivos netcdf con los datos y utilizan las funciones de `map_plots.py` para generar los graficos tipo pcolormesh, quiver y overlays.

En la carpeta `notebooks` se encuentran estos mismos scripts para en formato jupyter notebook.

En este repositorio no se incluye ningun archivo de datos, asi que si no se cuentan con los archivos el script fallara.


## Objetivo de los scripts

Con estos scripts tienen como objetivo la creación de los graficos estadisticos de los modelos de 
circulación, biogeoquimicos, oleaje y meteorologia del grupo de modelacion.

El listado de figuras a crear es el siguiente: 

1. **Climatológicas**

   Crear mapas de color de las siguientes variables y a estos niveles:

   - Graficos con de **Media** y **Desviación Estándar**

     - Niveles: 0 m, 50 m, 100 m, 250 m, 400 m, 600 m, 1000 m, 1500 m, 2000 m, 2500 m
  
       - Variables:
  
         - Temperatura (pcolormesh)
         - Salinidad (pcolormesh)
         - Magnitud de la velocidad (pcolormesh) + Capa de mapa de vectores (quiver)
         - Corrientes Vectores (quiver)  (**OPCIONAL**)
  
     - Superficie 
       
       - Variables:
  
         - Nivel del mar (pcolormesh)
         - Capa de mezcla (pcolormesh)  
       

2. **Mensuales**

   Crear mapas de color con datos estadisticos de las siguientes variables, en los siguientes meses, y estos niveles de profundidad.

   - Promedios, Maximos, Minimos, Desviacion Estandar
     
     - 12 meses, una figura para cada mes

       -  Niveles: 0 m, 50 m, 100 m, 250 m, 400 m, 600 m, 1000 m

          - Variables:
           
            - Temperatura (pcolormesh)
            - Salinidad (pcolormesh)
            - Magnitud de la velocidad (pcolormesh) + Capa de mapa de vectores (quiver) 


3. **Estacionales**

   Crear mapas de color solo en superficie de los datos estadisticos por estación.

   - Promedios y Desviación Estandar

     - Estaciones: Invierno, Primavera, Verano, Otoño

       - Variables:
  
         - Clorofila en superficie   (pcolormesh) 
         - Clorofila integrada en profundidad   (pcolormesh)


4. **Perfiles estacionales** (Modelos biogeoquimicos)

   Crear en una sola figura 4 perfiles para cada promedio de estación

   - Promedios

     - Zonas:

       - Bahia de campeche
       - Canal remolinos
       - Corriente de lazo
       - Norte canal remolinos

         - Estaciones: Invierno, Primavera, Verano, Otoño ( En una sola figura )

           - Variables:

             - Carbono
             - Oxígeno
             - Clorofila
             - Nitratos


## Estructura para almacenar figuras

```
 Figuras/
 ├── climatologicas/
 │   ├── capa-de-mezcla/
 │   │   ├── desviacion-estandar
 │   │   └── media
 │   ├── magnitud-velocidad-corrientes/
 │   │   ├── desviacion-estandar/
 │   │   └── media/
 │   ├── nivel-mar/
 │   │   ├── desviacion-estandar/
 │   │   └── media/
 │   ├── salinidad/
 │   │   ├── desviacion-estandar/
 │   │   └── media/
 │   ├── temperatura/
 │   │   ├── desviacion-estandar/
 │   │   └── media/
 │   └── corrientes-vectores/
 │       └── media/
 ├── mensual/
 |   ├── corrientes-vectores/
 │   |   └── promedios/
 |   ├── magnitud-velocidad-corrientes/
 |   │   ├── desviacion-estandar/
 |   │   ├── maximos/
 |   │   ├── minimos/
 |   │   └── promedios/
 |   ├── salinidad/
 |   │   ├── desviacion-estandar/
 |   │   ├── maximos/
 |   │   ├── minimos/
 |   │   └── promedios/
 │   └── temperatura/
 │       ├── desviacion-estandar/
 │       ├── maximos/
 │       ├── minimos/
 │       └── promedios/
 └── estacionales/
     ├── capa-de-mezcla/
     │   ├── desviacion-estandar/
     │   └── promedios/
     ├── oxigeno/
     │   ├── desviacion-estandar/
     │   ├── promedios/
     │   └── perfiles/
     ├── carbono/
     │   ├── desviacion-estandar/
     │   ├── promedios/
     │   └── perfiles/
     ├── nitratos/
     │   ├── desviacion-estandar/
     │   ├── promedios/
     │   └── perfiles/
     └── clorofila/
         ├── desviacion-estandar/
         ├── promedios/
         └── perfiles/

```

## Nomenclatura para nombres de archivos

El nombre del archivo de la figura, debe contener lo siguiente:

1. Comenzar por la clave del modelo asignada, 
2. Cada elemento de la nomenclatura va separado por un guion bajo `_`
2. El tipo de figura, puede ser clim, mensual, o estacional,
3. El nombre de la variable, puede ser capa-mezcla, nivel-mar, temperatura, salinidad, corrientes-vectores, magnitud-velocidad, nitratos, carbono o clorofila
4. La estadistica de la variable, puede ser media, desviacion-estandar, maximos, minimos, promedios
5. De incluir la profundad de la capa, se deben utilizar 4 digitos y el sufijo 'm' , ejemplo `0000m` o `2500m`
6. De incluir el mes de la figura, se deben utilizar siempre 2 digitos, y el prefijo `M`, ejemplo `M01` o `M12`


### Ejemplos de los nombres

   - Figuras climatológicas
  
      - Capa de mezcla

      ```
      [clave-modelo]_clim_capa-mezcla_media_0000m.png 
      [clave-modelo]_clim_capa-mezcla_media_0050m.png     
      ..
      .
      [clave-modelo]_clim_capa-mezcla_media_02500m.png 
      [clave-modelo]_clim_capa-mezcla_desvest_0000m.png 
      ```

      - Nivel del mar

      ```
      [clave-modelo]_clim_nivel-mar_media_0000m.png 
      ```

      - Temperatura
  
      ```
      [clave-modelo]_clim_temperatura_media_0000m.png 
      [clave-modelo]_clim_temperatura_media_0050m.png 
      ..
      .
      [clave-modelo]_clim_temperatura_media_2500m.png 
      ```
  
      - Salinidad
  
      ```
      [clave-modelo]_clim_temperatura_media_0000m.png 
      ..
      .
      [clave-modelo]_clim_temperatura_media_2500m.png 
      ```
  
      - Velocidad corrientes con vectores
      
      ```
      [clave-modelo]_clim_corrientes-vectores_promedio_0000m.png  
      ..
      .
      [clave-modelo]_clim_corrientes-vectores_promedio_2500m.png  
      ```

   - Figuras mensuales
  
      ```
      [clave-modelo]_mensual_temperatura_promedio_0000m_M01.png 
      ..
      .
      [clave-modelo]_mensual_temperatura_promedio_1000m_M12.png 
      [clave-modelo]_mensual_temperatura_desvest_0000m_M01.png 
      ...
      [clave-modelo]_mensual_temperatura_minimos_0000m_M01.png 
      ...
      [clave-modelo]_mensual_temperatura_maximos_0000m_M01.png 
      ...
  
      (Repetir formato para el resto de las variables)
      ```

   - Figuras estacionales

      ```
      [clave-modelo]_estacional_capa-mezcla_promedio_invierno_0000m.png    
      [clave-modelo]_estacional_capa-mezcla_promedio_primavera_0000m.png    
      [clave-modelo]_estacional_capa-mezcla_promedio_otono_0000m.png    
      [clave-modelo]_estacional_capa-mezcla_promedio_verano_0000m.png

      [clave-modelo]_estacional_capa-mezcla_desvest_verano_0000m.png
	     ...
       ..
      ``` 

   - Figuras estacionales Perfiles

      ```
      [clave-modelo]_estacional_carbono_promedio_bahia-campeche.png    
      [clave-modelo]_estacional_carbono_promedio_canal-remolinos.png    
      [clave-modelo]_estacional_carbono_promedio_corriente-lazo.png    
      [clave-modelo]_estacional_carbono_promedio_norte-canal-remolinos.png    

      [clave-modelo]_estacional_oxigeno_promedio_bahia-campeche.png    
      ...
      ```


## Solución a problemas


### Al ejecutar el script, no encuentra map_plots

Si al ejecutar el script les produce un error de que no encuentra el script map_plots.py,

```
ModuleNotFoundError: No module named 'map_plots'
```

El script map_plots.py se debe encontrar en el mismo directorio donde esté su su script, para que los `imports` si funcionen. 
De no estar en la misma carpeta hay que agregar la ruta donde se encuentra, agregando las siguientes líneas al inicio del script:

```
sys.path.append('./ruta-script/'))
from map_plots import map_pcolor, map_quiver, add_quiverPlot
```


### Los gráficos no se generan en mi terminal

Si el script esta produciendo un error asi:

```
RuntimeError: Invalid DISPLAY variable
```

Lo que sucede es que matplotlib intenta mostrar la figura, incluso al correr la instrucción `savefig`, y hay casos en donde no hay un servidor X atado a la terminal.  
En este caso podemos cambiar en backend gráfico. Agregando lo siguiente al principio del script:

```
import matplotlib
matplotlib.use('Agg')
```


### Error al agregar el plot add_quiverPlot

Errores del tipo:

```
ValueError: Argument u has a size XX which does not match YY, the number of arrow positions
```

De tener un error como el anterior, actualizar a la última versión del repositorio.  Se corrigio en el commit [bab94c22](https://gitlab.com/hmedrano/scripts-modelos-cigom/-/commit/bb3364d19853bea249f71e36637577dd86f0fee5)

Este error se generaba porque no se consideró que las variables de coordenadas lat, lon pueden ser 1D o 2D. El script asumia que solo eran 1D.

